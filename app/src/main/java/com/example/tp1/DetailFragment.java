package com.example.tp1;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.navigation.fragment.NavHostFragment;

import com.example.tp1.data.Country;

public class DetailFragment extends Fragment {

    public static final String TAG = "SecondFragment";
    TextView nom;
    ImageView drapeau;
    TextView capitale;
    TextView langue;
    TextView monnaie;
    TextView population;
    TextView superficie;

    @Override
    public View onCreateView(
            LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState
    ) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_second, container, false);
    }

    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        nom = view.findViewById(R.id.pays_nom);
        drapeau = view.findViewById(R.id.pays_drapeau);
        capitale = view.findViewById(R.id.pays_capitale);
        langue = view.findViewById(R.id.pays_langue);
        monnaie = view.findViewById(R.id.pays_monnaie);
        population = view.findViewById(R.id.pays_population);
        superficie = view.findViewById(R.id.pays_superficie);
        //// Implementation with bundle
        // textView.setText("Info in chapter "+(getArguments().getInt("numChapter")+1));

        DetailFragmentArgs args = DetailFragmentArgs.fromBundle(getArguments());
        //Maj du nom
        nom.setText(Country.countries[args.getCountryId()].getName());
        //Maj du drapeau
        Context c = getContext();
        drapeau.setImageDrawable(c.getResources().getDrawable((c.getResources().getIdentifier(Country.countries[args.getCountryId()].getImgUri() , null , c.getPackageName()))));
        nom.setText(Country.countries[args.getCountryId()].getName());
        //Maj de la capitale
        capitale.setText(Country.countries[args.getCountryId()].getCapital());
        //Maj de la langue
        langue.setText(Country.countries[args.getCountryId()].getLanguage());
        //Maj de la monnaie
        monnaie.setText(Country.countries[args.getCountryId()].getCurrency());
        //Maj de la population
        population.setText(""+Country.countries[args.getCountryId()].getPopulation());
        //Maj de la superficie
        superficie.setText(""+Country.countries[args.getCountryId()].getArea());

        view.findViewById(R.id.button_second).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                NavHostFragment.findNavController(DetailFragment.this)
                        .navigate(R.id.action_SecondFragment_to_FirstFragment);
            }
        });
    }
}